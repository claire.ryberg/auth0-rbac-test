# Managing Roles from Auth0 and viewing Role based content
This project is a POC to address this use case:

Allow a user to select their role after signing up with Auth0, rather than giving them a default role. Additionally, the user is also able to select the other role, or both roles, at a later time after signing up. 

A version of this question was asked [here](https://community.auth0.com/t/how-to-allow-user-to-choose-role-at-sign-up-react-serverless/53387)

## Live Demo
You can see this POC running [here](https://peaceful-boyd-837509.netlify.app/)

## Auth0 Rule
Part of the solution includes this Auth0 rule:
```javascript
// This is from this tutorial: https://auth0.com/blog/role-based-access-control-rbac-and-react-apps/
function (user, context, callback) {
  user.app_metadata = user.app_metadata || {};
  const namespace = 'https://auth0-rbac-test';
  
  if (typeof user.app_metadata.userType === 'undefined') {
  	user.app_metadata.userType = [];
  }
    
  auth0.users.updateAppMetadata(user.user_id, user.app_metadata)
  	.then(() => {
    	context.idToken[`${namespace}/userType`] = user.app_metadata.userType;
    	context.accessToken[`${namespace}/userType`] = user.app_metadata.userType;

    	callback(null, user, context);
  	})
  	.catch((err) => {
   		callback(err);
  	}
  );
}
```


## Available Scripts

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and [Netlify Dev](https://docs.netlify.com/cli/get-started/#netlify-dev)
It is best run locally with `netlify dev`
