const { verifyJwt } = require("../../lib/authorize");
const { requestManagementToken } = require("../../lib/requestManagementToken");
const fetch = require("node-fetch");

/**
 * This function validates a user accessToken (handled in verifyJwt)
 * Then it fetches a new M2M accessToken in order to use the Auth0 Management API
 * Next, it uses that new accessToken to sent a PATCH request to Auth0 to update the user's app metadata
 */
exports.handler = verifyJwt(async (event, context) => {
  // const access_token = event.headers.authorization //This is the user token
  const { claims } = context.identityContext;

  const access_token = await requestManagementToken();

  if (event.httpMethod === "PATCH") {
    const { userTypes } = JSON.parse(event.body);

    return await fetch(
      `${process.env.REACT_APP_AUTH0_MANAGEMENT_API}users/${claims.sub}`,
      {
        headers: {
          authorization: `Bearer ${access_token}`,
          "content-type": "application/json",
        },
        method: "PATCH",
        body: JSON.stringify({
          app_metadata: {
            userType: userTypes,
          },
        }),
      }
    )
      .then((res) => {
        if (!res.ok) {
          return {
            statusCode: res.status,
            body: JSON.stringify({
              error: `${res.status}: ${res.statusText}. Check userType server logs`,
            }),
          };
        }

        return res.json().then((data) => {
          const { app_metadata } = data;
          const updatedUserTypes = app_metadata.userType;

          if (updatedUserTypes.length === 0) {
            throw Error(`User ${data.user_id} is missing required 'userType'`);
          }

          return {
            statusCode: 200,
            body: JSON.stringify({
              userTypes: updatedUserTypes,
            }),
          };
        });
      })
      .catch((err) => {
        console.error(err.stack);

        return {
          statusCode: 500,
          body: JSON.stringify({
            error: `${err.name}: ${err.message}. Check userType server logs`,
          }),
        };
      });
  }
});
