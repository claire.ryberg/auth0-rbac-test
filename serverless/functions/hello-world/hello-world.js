// Docs on event and context https://www.netlify.com/docs/functions/#the-handler-method

//This function isn't being used anywhere. Keeping it just for reference
const handler = async (event) => {
  try {
    const subject = event.queryStringParameters.name || 'World'
    return {
      statusCode: 200,
      body: JSON.stringify({ message: `Hello ${subject}` }),
    }
  } catch (error) {
    return { statusCode: 500, body: error.toString() }
  }
}

module.exports = { handler }
