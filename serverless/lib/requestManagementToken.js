const fetch = require("node-fetch");

module.exports.requestManagementToken = async () => {
  return await fetch(
    `https://${process.env.REACT_APP_AUTH0_DOMAIN}/oauth/token`,
    {
      headers: {
        "content-type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        client_id: process.env.REACT_APP_AUTH0_MANAGEMENT_API_CLIENT_ID,
        client_secret: process.env.REACT_APP_AUTH0_MANAGEMENT_API_CLIENT_SECRET,
        audience: process.env.REACT_APP_AUTH0_MANAGEMENT_API,
        grant_type: "client_credentials",
      }),
    }
  )
    .then((res) => {
      if (!res.ok) {
        throw Error(`${res.status}, ${res.statusText}`);
      }

      return res.json().then(({ access_token }) => access_token);
    })
    .catch((err) => {
      console.error(err.stack);
    });
};
