export const fetchUserAppMetadata = async (accessToken, userId) => {
  // If there is no Auth0 rule in place to always return the userType, then you can reliably use this approach instead.
  // If the rule is set up, this isn't strictly necessary

  if(!accessToken || !userId) {
    return undefined
  }
  
  try {
    const metadataResponse = await fetch(`${process.env.REACT_APP_AUTH0_MANAGEMENT_API}users/${userId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    const metadata = await metadataResponse.json();
    const { app_metadata } = metadata;

    return app_metadata;
  } catch (e) {
    console.error(e.message);
    return undefined;
  }
};
