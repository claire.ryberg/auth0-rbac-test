import { USER_TYPE_A, USER_TYPE_B } from "../constants";

const AUTH0_RULES_DOMAIN = process.env.REACT_APP_AUTH0_RULES_DOMAIN;

export const getUserTypes = (user) => {
  if (!!user) {
    return user[`${AUTH0_RULES_DOMAIN}/userType`];
  }
  return undefined;
};

export const isUserTypeA = (typesArr) => {
  if (!typesArr) {
    return false;
  }
  return typesArr.indexOf(USER_TYPE_A) > -1;
};

export const isUserTypeB = (typesArr) => {
  if (!typesArr) {
    return false;
  }
  return typesArr.indexOf(USER_TYPE_B) > -1;
};
