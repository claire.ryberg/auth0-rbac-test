import React from "react";
import "./App.css";
import { useAuth0 } from "@auth0/auth0-react";

import AuthButton from "./components/AuthButton";
import SelectUserType from "./components/SelectUserType";

import { getUserTypes, isUserTypeA, isUserTypeB } from "./utils/get-user-types";
import { fetchUserAppMetadata } from "./utils/fetch-user-app-metadata";

function App() {
  const [helloMessage, setHelloMessage] = React.useState("");
  const [userTypeList, setUserTypeList] = React.useState([]);
  const [userTypeMessage, setUserTypeMessage] = React.useState("");
  const [accessToken, setAccessToken] = React.useState(null);

  const {
    user,
    isAuthenticated,
    isLoading,
    getAccessTokenSilently,
  } = useAuth0();

  React.useEffect(() => {
    if (isLoading) {
      setHelloMessage("Loading...");
      return;
    }

    if (isAuthenticated) {
      setHelloMessage(`Hello ${user.name}!`);

      getAccessTokenSilently().then((res) => {
        setAccessToken(res);
      });

      const userTypes = getUserTypes(user);

      // There should be an auth0 rule that adds userType to the user object
      // If that rule is broken or disabled, fetch userType manually
      if (!userTypes && !!user) {
        fetchUserAppMetadata(accessToken, user.sub).then((app_metadata) => {
          const fetchedUserTypes =
            (!!app_metadata && app_metadata.userType) || [];
          setUserTypeList(fetchedUserTypes);
        });
      } else {
        setUserTypeList(userTypes);
      }
    }

    if (!isAuthenticated) {
      setHelloMessage("Please log in!");
    }
  }, [
    user,
    accessToken,
    isAuthenticated,
    isLoading,
    getAccessTokenSilently,
    setAccessToken,
    setUserTypeList,
  ]);

  React.useEffect(() => {
    if(!isAuthenticated) {
      setUserTypeMessage("");
    }
    const msg =
      userTypeList.length > 0
        ? `User has these user types: ${userTypeList.join(" ")}`
        : "User has no userType";

    setUserTypeMessage(msg);
  }, [isAuthenticated, userTypeList, setUserTypeMessage]);

  const changeUserType = async (typesArray) => {
    const res = await fetch("/api/userType", {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      method: "PATCH",
      body: JSON.stringify({
        userTypes: typesArray,
      }),
    });

    const result = await res.json();
    if (result.error) {
      console.error(result.error);
    } else {
      setUserTypeList(result.userTypes);
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <p>{helloMessage}</p>
        {!isLoading && (
          <AuthButton />
        )}
        {isAuthenticated && (
          <>
            <p>{userTypeMessage}</p>
            <SelectUserType submitCallback={changeUserType} />
          </>
        )}
        {isUserTypeA(userTypeList) && (
          <p>Content only shown for userTypeA folks!</p>
        )}
        {isUserTypeB(userTypeList) && (
          <p>Content only shown for userTypeB peoples!</p>
        )}
      </header>
    </div>
  );
}

export default App;
