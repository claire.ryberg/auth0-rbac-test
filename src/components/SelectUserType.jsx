import React from "react";
import { USER_TYPE_A, USER_TYPE_B } from "../constants";

const SelectUserType = ({ submitCallback }) => {
  const [typeAIsChecked, setTypeAIsChecked] = React.useState(false);
  const [typeBIsChecked, setTypeBIsChecked] = React.useState(false);

  const onTypeAChange = () => {
    setTypeAIsChecked(!typeAIsChecked);
  };

  const onTypeBChange = () => {
    setTypeBIsChecked(!typeBIsChecked);
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    let typesArr = [];
    if (typeAIsChecked) {
      typesArr.push(USER_TYPE_A);
    }

    if (typeBIsChecked) {
      typesArr.push(USER_TYPE_B);
    }

    submitCallback(typesArr);
  };

  return (
    <form noValidate onSubmit={handleFormSubmit}>
      <fieldset>
        <legend>Change your user type</legend>
        <label htmlFor="userTypeA">
          <input
            type="checkbox"
            id="userTypeA"
            name="userTypeA"
            checked={typeAIsChecked}
            onChange={onTypeAChange}
          />
          User Type A
        </label>
        <br />
        <label htmlFor="userTypeB">
          <input
            type="checkbox"
            id="userTypeB"
            name="userTypeB"
            checked={typeBIsChecked}
            onChange={onTypeBChange}
          />
          User Type B
        </label>
        <br />
        <button>Save</button>
      </fieldset>
    </form>
  );
};

export default SelectUserType;
